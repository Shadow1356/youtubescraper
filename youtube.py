from googleapiclient.discovery import build
import tkinter
import datetime
import tkcalendar
import pprint
import os


class GUI(tkinter.Frame):
	def __init__(self, master):
		super().__init__(master)
		self.window_sizes = {True: "1000x400", False: "800x100"}
		master.geometry(self.window_sizes[False])
		master.option_add("*Font", ("Arial", 15))
		#Simple Search
		self.search_term_input = tkinter.Entry(master)
		self.channel_check_var = tkinter.IntVar()
		self.video_check_var = tkinter.IntVar()
		self.channel_check_var.set(1)
		self.video_check_var.set(1)
		self.video_check = tkinter.Checkbutton(master, text="Videos", variable=self.video_check_var)
		self.channel_check = tkinter.Checkbutton(master, text="Channels", variable=self.channel_check_var)
		self.search_button = tkinter.Button(master, text="Search")
		#Advanced search
		self.advanced_button = tkinter.Button(master, text="Advanced Search")
		self.advanced_value = False
		#Location
		self.location_check_var = tkinter.IntVar()
		self.location_check = tkinter.Checkbutton(master, text="Filter Location", variable=self.location_check_var)
		self.lat_input = tkinter.Entry(master, state="disabled")
		self.long_input = tkinter.Entry(master, state="disabled")
		self.radius_input = tkinter.Entry(master, state="disabled")
		radius_measurements = ["mi", "ft", "km", "m"]
		self.measurement_var = tkinter.StringVar()
		self.measurement_var.set(radius_measurements[0])
		radius_arg = [master, self.measurement_var, *radius_measurements]
		self.radius_measurement_dropdown = tkinter.OptionMenu(*radius_arg)
		self.radius_measurement_dropdown.configure(state="disabled")
		#Order By
		order_options = ["date", "rating", "relevance", "title", "viewCount"]
		self.order_var = tkinter.StringVar()
		self.order_var.set(order_options[0])
		order_arg = [master, self.order_var, *order_options]
		self.order_dropdown = tkinter.OptionMenu(*order_arg)
		#Date Filtering
		self.date_check_var = tkinter.IntVar()
		self.date_check = tkinter.Checkbutton(master, text="Filter Date", variable=self.date_check_var)
		default_end_date = datetime.date.today()
		default_start_date = default_end_date - datetime.timedelta(weeks=2)
		self.date_before_value = tkinter.StringVar()
		pattern = "%Y-%m-%d"
		self.date_before_value.set(default_end_date.strftime(pattern))
		self.date_after_value = tkinter.StringVar()
		self.date_after_value.set(default_start_date.strftime(pattern))
		self.date_before_input = tkinter.Button(master, textvariable=self.date_before_value, state="disabled")
		self.date_after_input = tkinter.Button(master, textvariable=self.date_after_value, state="disabled")
		#Video Duration
		self.duration_check_var = tkinter.IntVar()
		self.duration_check = tkinter.Checkbutton(master, text="Filter By Video Length", variable=self.duration_check_var)
			#Will have to apply more processing after selection
		duration_options = ["Shorter than 4 minutes", "Longer than 20 minutes", "In between"]
		self.duration_option_var = tkinter.StringVar()
		self.duration_option_var.set(duration_options[0])
		duration_arg = [master, self.duration_option_var, *duration_options]
		self.duration_dropdown = tkinter.OptionMenu(*duration_arg)
		self.duration_dropdown.configure(state="disabled")

		master.title("Youtube Scraper - Ashland University")
		self.place_gui()
		self.add_functionality()
		self.setup_youtube()

	def setup_youtube(self):
		DEVELOPER_KEY = 'AIzaSyAlGqoehP52Iq6EWH80sjgnUPSOXhwtFRM'
		YOUTUBE_API_SERVICE_NAME = 'youtube'
		YOUTUBE_API_VERSION = 'v3'
		self.youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
			developerKey=DEVELOPER_KEY)


	def location_activate(self, var, *elements):
		if self.channel_check_var.get():
			print("Cannot add those advanced options to a channel search.")
			var.set(0)
			return
		self.activate_widgets(var, *elements)

	def add_functionality(self):
		#Advanced Button draws the advanced widgets
		self.advanced_button.configure(command=self.advanced_hit)
		#Checkbutton Disable stuff
		self.location_check.configure(command=lambda:self.location_activate(self.location_check_var,
		 	self.lat_input, self.long_input, self.radius_input, self.radius_measurement_dropdown))
		self.date_check.configure(command=lambda:self.location_activate(self.date_check_var,
			self.date_before_input, self.date_after_input))
		self.duration_check.configure(command=lambda:self.location_activate(self.duration_check_var,
			self.duration_dropdown))
		#Date Popups
		self.date_before_input.configure(command=lambda:self.popup_date(self.date_before_value))
		self.date_after_input.configure(command=lambda:self.popup_date(self.date_after_value))
		#Search Button
		self.search_button.configure(command=self.search_action)

	def search_action(self):
		term = self.search_term_input.get()
		if term == "":
			return
		#Type
		typestr = ""
		if self.channel_check_var.get() and self.video_check_var.get():
			typestr = "channel,video"
		elif self.channel_check_var.get():
			typestr = "channel"
		elif self.video_check_var.get():
			typestr = "video"
		args = {"q": term, "type":typestr, "part":"snippet", "maxResults" : 50}
		#Advanced Search options
		if self.advanced_value:
			#Order By
			args["order"] = self.order_var.get()
			#Location
			if self.location_check_var.get():
				lat = self.lat_input.get()
				longitude = self.long_input.get()
				radius = self.radius_input.get()
				measurement = self.measurement_var.get()
				args["location"] = lat + "," + longitude
				args["locationRadius"] = radius + measurement
			#Date
			if self.date_check_var.get():
				date_before = self.date_before_value.get()
				print(date_before)
				before_formatted = self.get_rfc3339(date_before)
				print(before_formatted)
				date_after = self.date_after_value.get()
				print(date_after)
				after_formatted = self.get_rfc3339(date_after)
				print(after_formatted)
				args["publishedBefore"] = before_formatted
				args["publishedAfter"] = after_formatted
			#Video Length
			if self.duration_check_var.get():
				duration = self.duration_option_var.get()
				duration_code = ""
				if duration == "Shorter than 4 minutes":
					duration_code = "short"
				elif duration == "Longer than 20 minutes":
					duration_code = "long"
				elif duration == "In between":
					duration_code = "medium"
				args["videoDuration"] = duration_code
		response = self.youtube.search().list(**args).execute()
		# pprint.pprint(response)
		self.process(response)
		try:
			os.system('start /B notepad.exe youtube_results.txt')
		except Exception:
			print("not windows")


	def iso_duration_convert(self, duration_str):
		isLetter = True
		number_str = ""
		duration_array = []
		for char in duration_str:
			if char.isdigit():
				number_str += char
				isLetter = False
			else:
				if not isLetter:
					duration_array.append(int(number_str))
					number_str = ""
				isLetter = True
		return duration_array

	def process(self, data):
		results = data["items"]
		video_url = "www.youtube.com/watch?v="
		channel_url = "www.youtube.com/channel/"
		with open("youtube_results.txt", 'w', encoding="utf-8") as file:
			size_str = "There were " + str(len(results)) + " results for the query." + "\n\n"
			file.write(size_str)
			for item in results:
				# pprint.pprint(item)
				if not "id" in item:
					# This is a "topic", and not what we want
					continue
				item_id = item["id"]
				if item_id["kind"] == "youtube#video":
					video_id = item_id["videoId"]
					item_information = item["snippet"]
					rfc3339_date = item_information["publishedAt"]
					normal_date = rfc3339_date.split("T")[0]
					channel_title = item_information["channelTitle"]
					video_title = item_information["title"]
					video_description = item_information["description"]
					# print(video_id)
					# print(rfc3339_date)
					# print(normal_date)
					# print(channel_title)
					# print(video_title)
					# print(video_description)
					

					# Get the remaining information
					video_response = self.youtube.videos().list(
						id=video_id,
						part="snippet, contentDetails, statistics"
						).execute()
					pprint.pprint(video_response)
					video_data = video_response["items"][0]
					#Description 
					if "description" in video_data["snippet"]:
						# print("VIDEO DESCRIPTION")
						# print(video_data["snippet"]["description"])
						video_description = video_data["snippet"]["description"].replace("\n", "   ")
					text = "Video Search Result: " + video_title + "\n" + \
						"By " + channel_title + " on " + normal_date + "\n"+\
						video_url + video_id + "\n\n" +\
						"Description: " + video_description + "\n\n"
					#Duration
					if "duration" in video_data["contentDetails"]:
						duration_split = self.iso_duration_convert(video_data["contentDetails"]["duration"])
						size = len(duration_split)
						duration_text =""
						for i in range(1, size+1):
							t = duration_split[-i]
							if i == 1:
								duration_text += str(t) + " seconds."
							elif i==2:
								duration_text = str(t) + " minutes " + duration_text
							elif i==3:
								duration_text = str(t) + " hours " + duration_text
							elif i==4: 
								duration_text = str(t) + " days " + duration_text
						text += "Duration is " + duration_text + "\n"
					#Tags
					if "tags" in video_data["snippet"]:
						text += "Tags Used: " + str(video_data["snippet"]["tags"]) + "\n"
					# Stats:
					stats = video_data["statistics"]
					if "viewCount" in stats:
						text += "Number of Views " + stats["viewCount"] + "\n"
					if "commentCount" in stats:
						text += "Number of Comments " + stats["commentCount"] + "\n"
					if "likeCount" in stats:
						text += "Number of Likes " + stats["likeCount"] + "\n"
					if "dislikeCount" in stats:
						text += "Number of Dislikes " + stats["dislikeCount"] + "\n"
					if "favoriteCount" in stats:
						text += "Number of Favorites " + stats["favoriteCount"] + "\n"
					text += "\n\n\n\n\n"
					file.write(text)
				elif item_id["kind"] == "youtube#channel":
					channel_id = item_id["channelId"]
					item_information = item["snippet"]
					channel_title = item_information["channelTitle"]
					rfc3339_start_date = item_information["publishedAt"]
					normal_start_date = rfc3339_start_date.split("T")[0]
					channel_description = item_information["description"]
					channel_response = self.youtube.channels().list(
						id=channel_id,
						part="snippet, statistics"
						).execute()
					pprint.pprint(channel_response)
					channel_data = channel_response["items"][0]
					#Description
					if "description" in channel_data["snippet"]:
						channel_description = channel_data["snippet"]["description"].replace("\n", "   ")
					text = "Channel Search Result: " + channel_title + "\n" + \
						"Created on " + normal_start_date + "\n"+\
						channel_url + channel_id + "\n\n" +\
						"Description: " + channel_description + "\n\n"
					#Stats:
					stats = channel_data["statistics"]
					if "videoCount" in stats:
						text += "Number of Videos " + stats["videoCount"] + "\n"
					if "viewCount" in stats:
						text += "Total Number of Views " + stats["viewCount"] + "\n"
					if "subscriberCount" in stats:
						text += "Number of Subscribers to the Channel " + stats["subscriberCount"] + "\n"
					text += "\n\n\n\n\n"
					file.write(text)


	def get_rfc3339(self, date_str):
		date_args = date_str.split("-")
		datetime_obj = datetime.datetime(int(date_args[0]), int(date_args[1]), int(date_args[2]))
		timestamp = datetime_obj.timestamp()
		utc = datetime.datetime.utcfromtimestamp(timestamp)
		end_format  = utc.isoformat("T") + "Z"
		return end_format

	def advanced_hit(self):
		self.advanced_value = not self.advanced_value
		self.master.geometry(self.window_sizes[self.advanced_value])
		self.place_advanced()


	def popup_date(self, value_var):
		def ok_exit():
			nonlocal calendar, value_var, popup
			dateOBJ = calendar.selection_get()
			value_var.set(dateOBJ.strftime("%Y-%m-%d"))
			popup.destroy()

		popup = tkinter.Toplevel()
		calendar = tkcalendar.Calendar(popup, selectbackground="purple")
		calendar.grid(row=0, column=0)
		tkinter.Button(popup, text="OK", command=ok_exit).grid(row=1, column=0)
		tkinter.Button(popup, text="Cancel", command=popup.destroy).grid(row=1, column=1)

	def activate_widgets(self, var, *elements):
		button_state = var.get()
		for widget in elements:
			if button_state == 1:	
				widget.configure(state="normal")
			else:
				widget.configure(state="disabled")

	def place_gui(self):
		#Row 0
		tkinter.Label(self.master, text="Search For:").grid(row=0, column=0)
		self.search_term_input.grid(row=0, column=1)
		self.channel_check.grid(row=0, column=2)
		self.video_check.grid(row=0, column=3)
		self.search_button.grid(row=0, column=4)
		# Row 1
		self.advanced_button.grid(row=1, column=0)
		self.advanced_button.configure(foreground='blue')
	
	def place_advanced(self):
		if self.advanced_value:
			#Row 2
			tkinter.Label(self.master, text="Order By:").grid(row=2, column=0)
			self.order_dropdown.grid(row=2, column=1)
			#### Location ####
			tkinter.Label(self.master).grid(row=3)
			#Row 3
			self.location_check.grid(row=4, column=0)
			tkinter.Label(self.master, text="Latitude").grid(row=4, column=1)
			self.lat_input.grid(row=4, column=2)
			#Row 4
			tkinter.Label(self.master, text="Longitude").grid(row=5, column=1)
			self.long_input.grid(row=5, column=2)
			#Row 5
			tkinter.Label(self.master, text="Radius").grid(row=6, column=1)
			self.radius_input.grid(row=6, column=2)
			self.radius_measurement_dropdown.grid(row=6, column=3)
			#######
			tkinter.Label(self.master).grid(row=7)
			#Row 6
			self.date_check.grid(row=8, column=0)
			tkinter.Label(self.master, text="After").grid(row=8, column=1, sticky='E', padx=20)
			self.date_after_input.grid(row=8, column=2, sticky='W')
			tkinter.Label(self.master, text="Before").grid(row=8, column=3)
			self.date_before_input.grid(row=8, column=4)

			tkinter.Label(self.master).grid(row=9)
			# Row 7
			self.duration_check.grid(row=10, column=0)
			self.duration_dropdown.grid(row=10, column=2)
		else: #remove all widgets in row 2 and above. (or below....)
			for widget in self.master.grid_slaves():
				if int(widget.grid_info()['row']) >=2:
					widget.grid_forget()



if __name__ == "__main__":
	root = tkinter.Tk()
	gui = GUI(root)
	gui.mainloop()

